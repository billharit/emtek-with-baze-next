import Link from "next/link";

export const Card = ({ data, path }) => (
  <div className="news-card">
    <Link href={`${path}${data?.slug}`}>
      <a>{data?.title}</a>
    </Link>
  </div>
);
