import Image from "next/image";
import Link from "next/link";
import logo from "~/public/static/img/logo-emtek.png";

export const Logo = () => (
  <Link href="/">
    <a>
      <Image src={logo.src} alt="Logo Emtek" width={125} height={50} />
    </a>
  </Link>
);
