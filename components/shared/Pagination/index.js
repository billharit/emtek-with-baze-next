import { useEffect, useRef, useState } from "react";
import style from "./pagination.module.scss";

const Pagination = ({ maxVisibleButtons = 5, totalPages, perPage, currentPage, onPageChange }) => {
  const [pages, setPages] = useState([]);

  const endPage = useRef();
  const startPage = useRef();

  useEffect(() => {
    if (currentPage === 1) {
      startPage.current = 1;
    } else if (currentPage === totalPages) {
      startPage.current = totalPages - maxVisibleButtons + 1;
    } else {
      startPage.current = currentPage - 1;
    }

    endPage.current = Math.min(startPage.current + maxVisibleButtons - 1, totalPages);

    const range = [];
    for (let i = startPage.current; i <= endPage.current; i += 1) {
      range.push({
        name: i,
        isDisabled: i === currentPage
      });
    }

    setPages(range);
    console.log(range);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onClickFirstPage = () => onPageChange(1);
  const onClickPage = page => onPageChange(page);
  const onClickLastPage = () => onPageChange(totalPages);
  const onClickPreviousPage = () => onPageChange(currentPage - 1);
  const onClickNextPage = () => onPageChange(currentPage + 1);
  const isPageActive = page => currentPage == page;

  return (
    <ul className={"pagination"}>
      <li className={style["pagination-item"]}>
        <button
          className={`${style["btn-arrow"]} ${style["button"]}`}
          aria-label="Go to first page"
          disabled={currentPage === 1}
          onClick={onClickFirstPage}
        >
          <span className="bzi-angle-double-left"></span>
        </button>
      </li>

      <li className={style["pagination-item"]}>
        <button
          className={`${style["btn-arrow"]} ${style["button"]}`}
          aria-label="Go to previous page"
          disabled={currentPage === 1}
          onClick={onClickPreviousPage}
        >
          <span className="bzi-angle-left"></span>
        </button>
      </li>

      {pages
        .filter(p => p.name > 0)
        .map((page, idx) => (
          <li className={style["pagination-item"]} key={idx}>
            {page.name !== 0 && (
              <button
                className={`${style["btn-page"]} ${style["button"]}`}
                disabled={page.isDisabled}
                aria-label={`Go to page number ${page.name}`}
                onClick={onClickPage(page.name)}
              >
                {page.name}
              </button>
            )}
          </li>
        ))}

      <li className={style["pagination-item"]}>
        <button
          className={`${style["btn-arrow"]} ${style["button"]}`}
          disabled={currentPage === totalPages}
          aria-label="Go to next page"
          onClick={onClickNextPage}
        >
          <span className="bzi-angle-right"></span>
        </button>
      </li>

      <li className={style["pagination-item"]}>
        <button
          className={`${style["btn-arrow"]} ${style["button"]}`}
          disabled={currentPage === totalPages}
          aria-label="Go to last page"
          onClick={onClickLastPage}
        >
          <span className="bzi-angle-double-right"></span>
        </button>
      </li>
    </ul>
  );
};

export default Pagination;
