import useAxios from "~/hooks/useAxios";
import Link from "next/link";
import style from "./footer.module.scss";

export const Footer = () => {
  const { response } = useAxios({
    method: "GET",
    url: "/static/footers.json"
  });

  return (
    <header className={style["site-footer"]}>
      <div className="container footer-container">
        <div className="flex-responsive  v-center--spread">
          {response?.map(({ title, items }) => (
            <ul className="list-nostyle" key={title}>
              <li className="text-semibold mb-8">{title}</li>
              {items.map(({ item, link }) => (
                <li className={style["list-item-style"] + " " + "mt-8"} key={item}>
                  <Link href={link}>
                    <p>{item}</p>
                  </Link>
                </li>
              ))}
            </ul>
          ))}
        </div>
        <hr className="mt-16 mb-16" />{" "}
        <div className="text-center">
          <Link href="/">
            <p>Copyright ©2022 PT. Elang Mahkota Teknologi Tbk. All Right Reserved</p>
          </Link>
        </div>
      </div>
    </header>
  );
};
