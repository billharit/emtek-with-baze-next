import { render } from "@testing-library/react";
import Footer from "..";

describe("Footer", () => {
  it("should be rendered", () => {
    const { container } = render(<Footer />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
