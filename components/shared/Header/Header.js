import { Logo } from "~/components/shared/Logo";
import { HeaderNav } from "./HeaderNav";

import style from "./header.module.scss";

export const Header = () => (
  <header className={style["site-header"]}>
    <div className="container">
      <Logo />
      <HeaderNav />
    </div>
  </header>
);
