import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";
import style from "./header.module.scss";

export const MainNav = ({ mainNavData }) => {
  const { status } = useSession();
  return (
    <nav className={style["main-nav"]}>
      <ul className="list-nostyle">
        {mainNavData?.map(({ title, url }) => (
          <li className="main-nav__item" key={title}>
            <Link href={url}>
              <a className={style["header-link"]}>{title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};
