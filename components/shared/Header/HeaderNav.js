import { MainNav } from "./MainNav";

import useToggle from "~/hooks/useToggle";
import useWindowSize from "~/hooks/useWindowSize";
import useAxios from "~/hooks/useAxios";

import style from "./header.module.scss";

export const HeaderNav = () => {
  const [width] = useWindowSize();
  const [showNav, toggleShowNav] = useToggle(false);
  const { response } = useAxios({
    method: "GET",
    url: "/static/menus.json"
  });

  return (
    <div className={style["header-nav"]}>
      <button
        type="button"
        aria-label="Menu"
        onClick={toggleShowNav}
        className={`${style["nav-toggle"]} btn--ghost-grey `}
      >
        <span aria-hidden="true" className="bzi-bars bzi-1_2x"></span>
      </button>
      {(showNav || width > 768) && (
        <div className={style["nav-container"]}>
          <MainNav mainNavData={response} />
          <div></div>
        </div>
      )}
    </div>
  );
};
