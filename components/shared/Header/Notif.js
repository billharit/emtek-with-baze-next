import style from "./header.module.scss";

export const Notif = () => (
  <div className={style["header-notif"]}>
    <a href="/notifikasi" className={style["header-link"]}>
      <span className="bzi-notification bzi-1_2x"></span>
    </a>
  </div>
);
