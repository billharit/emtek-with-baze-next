import Head from "next/head";
import { useRouter } from "next/router";
import { defaultMeta } from "~/config/defaultMeta";

const MetaHead = ({ customMeta }) => {
  const router = useRouter();
  const meta = {
    ...defaultMeta,
    ...customMeta
  };
  return (
    <Head>
      <title>{meta.title}</title>
      <meta name="description" content={meta.description} />
      <meta name="keywords" content={meta.keywords} />
      <meta name="theme-color" content="#ffffff" />

      {/* Open Graph */}
      <meta name="title" property="og:title" content={meta.title} />
      <meta name="description" property="og:description" content={meta.description} />
      <meta name="url" property="og:url" content={`${meta.url}${router.asPath}`} />
      <meta name="type" property="og:type" content={meta.type} />
      <meta name="site_name" property="og:site_name" content={meta.title} />
      <meta name="image" property="og:image" content={meta.image} />

      <meta property="fb:app_id" content="" />

      {/* Twitter */}
      <meta name="twitter:title" content={meta.title} />
      <meta name="twitter:description" content={meta.description} />
      <meta name="twitter:image" content={meta.image} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@suitmedia" />
      <meta name="twitter:creator" content="@suitmedia" />

      <link rel="canonical" href={`${meta.url}${router.asPath}`} />
    </Head>
  );
};

export default MetaHead;
