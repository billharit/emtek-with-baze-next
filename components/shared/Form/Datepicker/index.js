import { Controller } from "react-hook-form";
import DatePicker from "react-datepicker";

export const ReactDatepicker = ({ control, name, rules = null, ...rest }) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { value, onChange, onBlur } }) => (
        <DatePicker onBlur={onBlur} selected={value} onChange={onChange} {...rest} />
      )}
    />
  );
};
