import { Controller, Control, FieldValues } from "react-hook-form";
import Select from "react-select";
import { arrayHasKey } from "~/utils";

/**
 * Simplified version of react-hook-form + react-select.
 * @param {Object} param
 * @param {Control<FieldValues, any>} param.control Control instance returned from the useForm hook by react-hook-form
 * @param {Array} param.values The options for the select input, can be an array of object that has label and value key for the values, or an array that contains the list of the values
 * @param {String} param.name The name for the selected value object
 * @param {Object?} param.rules Rules for the validation. See https://react-hook-form.com/api/useform/control#rules for details
 * @param {String?} param.placeholder The placeholder for the select input
 * @returns
 */
export const ReactSelect = ({ control, values, name, rules = null, placeholder = "", ...rest }) => {
  const options =
    arrayHasKey(values, "label") && arrayHasKey(values, "value")
      ? values
      : values?.map(value => ({
          label: value,
          value
        }));

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { value, onChange, onBlur, ref } }) => {
        return (
          <Select
            id="selectbox"
            instanceId="selectbox"
            options={options}
            placeholder={placeholder}
            onChange={opts => onChange(opts?.value)}
            onBlur={onBlur}
            value={value ? options?.find(c => c.value === value) : null}
            // defaultValue={options?.filter(option => value?.includes(option.value))}
            {...rest}
          />
        );
      }}
    />
  );
};
