import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Link from "next/link";

import { toURLFormat } from "~/utils/index";
import style from "./breadcrumbs.module.scss";

const BreadCrumbs = () => {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    if (router) {
      const fullPath = router.asPath.split("?")[0]; // without query params
      const paths = fullPath.split("/").filter(v => v.length > 0);

      const titles = [];
      const urls = [];

      paths.forEach((v, i) => {
        v = v.split("-").join(" ");
        titles.push(v);

        const url = titles.slice(0, i + 1).toString();

        urls.push({
          title: v,
          href: "/" + toURLFormat(url.split(",").join("/"))
        });
      });

      setBreadcrumbs(urls);
    }
  }, [router]);

  return (
    <ul className={`list-nostyle ${style["breadcrumbs"]}`}>
      <li>
        <Link href="/">
          <a>
            <i className={`${style["icon-crumbs"]} bzi-home`}></i>
          </a>
        </Link>
      </li>
      {breadcrumbs?.map((v, i) => (
        <li key={v}>
          <span className={`${style["icon-crumbs"]} bzi-angle-right`}></span>
          <Link href={v.href}>{v.title}</Link>
        </li>
      ))}
    </ul>
  );
};

export default BreadCrumbs;
