import { render } from "@testing-library/react";
import BreadCrumbs from "..";

describe("BreadCrumbs", () => {
	it("should be rendered", () => {
		const { container } = render(<BreadCrumbs />);
		expect(container.firstChild).toMatchSnapshot();
	});
});
