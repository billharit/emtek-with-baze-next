import React, { useState, forwardRef, useImperativeHandle } from "react";
import ClientOnlyPortal from "../ClientOnlyPortal";

import style from "./modal.module.scss";

export const ModalFooter = ({ children }) => children && <div className={style["modal-footer"]}>{children}</div>;

const Modal = forwardRef(({ size = "large", closeAction = true, children, ...rest }, ref) => {
	const [display, setDisplay] = useState(false);

	useImperativeHandle(ref, () => {
		return {
			open: () => show(),
			hide: () => hide()
		};
	});

	const show = () => {
		setDisplay(true);
	};

	const hide = () => {
		setDisplay(false);
	};

	return (
		<>
			{display && (
				<ClientOnlyPortal selector={"#modal-area"}>
					<div className={style["modal-wrapper"]}>
						<div onClick={hide} className={style["modal-backdrop"]} />
						<div
							className={`${style["modal-box"]} ${style[`modal--${size}`]} ${
								closeAction ? "" : style["modal--no-close"]
							}`}
							{...rest}
						>
							{closeAction && (
								<button className={style["btn-close"]} onClick={hide}>
									<span className="bzi-close"></span>
								</button>
							)}
							<div className={style["modal-content"]}>{children}</div>
							<ModalFooter />
						</div>
					</div>
				</ClientOnlyPortal>
			)}
		</>
	);
});

Modal.displayName = "Modal";

export default Modal;
