import style from "./profileanimation.module.scss";
import { motion } from "framer-motion";
import Marquee from "./Marquee";

export const ProfileAnimation = () => (
  <div className={style["animation-container"]}>
    <Marquee />
  </div>
);
