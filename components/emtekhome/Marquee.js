import React from "react";
import { motion } from "framer-motion";
import { useState, useEffect } from "react";
import useWindowSize from "~/hooks/useWindowSize";

import style from "./marquee.module.scss";

const Marquee = () => {
  const [width] = useWindowSize();

  const marqueeVariants = {
    animate: {
      x: [0, -width],
      transition: {
        x: {
          repeat: Infinity,
          repeatType: "loop",
          duration: 15,
          ease: "linear"
        }
      }
    }
  };

  return (
    <div>
      <div className={style["marquee"]}>
        <motion.div className={style["track"]} variants={marqueeVariants} animate="animate">
          <h1>
            EMTEK KEY FIGURE <span>EMTEK KEY FIGURE</span> EMTEK KEY FIGURE <span>EMTEK KEY FIGURE</span>
          </h1>
        </motion.div>
      </div>
    </div>
  );
};

export default Marquee;
