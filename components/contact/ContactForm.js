import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { ReactSelect } from "~/components/shared/Form/Select";
import { ReactDatepicker } from "~/components/shared/Form/Datepicker";

import style from "./style.module.scss";

const ContactForm = () => {
  const {
    register,
    handleSubmit,
    control,
    reset,
    watch,
    formState: { errors }
  } = useForm({
    shouldUnregister: true
  });

  const onSubmit = data => {
    alert(JSON.stringify(data));
    console.log("submitted : ", data);
    reset({
      gender: undefined,
      nut: undefined,
      name: ""
    });
  };

  const options = [
    { value: "male", label: "Male" },
    { value: "female", label: "Female" },
    { value: "no", label: "Prefer not to say" }
  ];

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={style["form-wrapper"]}>
      <div className="bzg">
        <div className="bzg_c pb-12 pl-0" data-col="m12">
          <ReactSelect
            control={control}
            values={options}
            rules={{ required: "There is no way you dont have any gender!" }}
            name="gender"
            placeholder="Whats your gender?"
          />
          <span className={style["error-msg"]}>{errors.gender?.message}</span>
        </div>

        <div className="bzg_c pb-12 pl-0" data-col="m12">
          <ReactSelect
            control={control}
            values={["peanut", "bean", "nutella"]}
            name="food"
            rules={{ required: "this field is required" }}
            placeholder="Favorite food?"
          />
          <span className={style["error-msg"]}>{errors.food?.message}</span>
        </div>

        <div className="bzg_c pb-12 pl-0" data-col="m4">
          <ReactDatepicker name={"date"} className="form-input mr-8" control={control} />
        </div>

        <div className="bzg_c" data-col="m4">
          <input
            className="form-input mr-8"
            {...register("name", {
              required: "Your name please?",
              minLength: { value: 3, message: "Your name is too short :(" }
            })}
          />
          <span className={style["error-msg"]}>{errors.name?.message}</span>
        </div>

        <div className="bzg_c pl-0">
          <input type="submit" className="btn--primary mr-12" />
          <input
            type="reset"
            className="btn--ghost-primary"
            onClick={() =>
              reset({
                gender: undefined,
                nut: undefined,
                name: ""
              })
            }
          />
        </div>
      </div>
    </form>
  );
};

export default ContactForm;
