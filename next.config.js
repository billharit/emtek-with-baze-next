const path = require("path");

/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	trailingSlash: false,
	sassOptions: {
		includePaths: [path.join(__dirname, "styles/partials/")],
		prependData: `@import "_variables.scss";` // prepend the variables file so it can be used by every scss file
	},
	images: {
    domains: ['assets.suitdev.com'],
  },
	async rewrites() {
		return [
			{
				source: "/base/:path*",
				destination: `${process.env.BASE_URL}/:path*`
			}
		];
	}

	//another option for redirection
	//async redirects() {
	//return [
	//{
	//source: "/contact-us",
	//destination: "/about-us",
	//permanent: true
	//}
	//];
	//}
};

module.exports = nextConfig;
