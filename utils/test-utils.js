import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

import rootReducers from "~/redux/reducers";

export const MockReduxProvider = props => (
	<Provider store={configureStore({ reducer: rootReducers })}>{props.children}</Provider>
);
