export const months = {
  id: {
    short: "Jan_Feb_Mar_Apr_Mei_Jun_Jul_Agt_Sep_Okt_Nov_Des".split("_"),
    full: "Januari_Februari_Maret_April_Mei_Juni_Juli_Augustus_September_Oktober_November_Desember".split("_")
  },
  en: {
    short: "Jan_Feb_Mar_Apr_May_Jun_Jul_Agt_Sep_Oct_Nov_Dec".split("_"),
    full: "January_February_March_April_May_June_July_August_September_October_November_December".split("_")
  }
};

/**
 * Format the date from string to the prefered:
 * - month length (full or short)
 * - lang (en or id)
 * - monthOnly (example if true and other parameters are default: Januari 2022. If false: 17 Januari 2022)
 * @param {String} date
 * @param {String} monthLength
 * @param {String} lang
 * @param {Boolean} monthOnly
 * @returns {String}
 */
export const formatDate = (date, monthLength = "short", lang = "id", monthOnly = false) => {
  const parseDate = new Date(date.replace(/-/g, "/"));
  let day = "";
  if (!monthOnly) {
    day = parseDate.getDate();
  }
  const month = parseDate.getMonth();
  const year = parseDate.getFullYear();
  const monthName = months[lang][monthLength][month];
  let format = `${monthName} ${year}`;
  if (!monthOnly) {
    format = `${monthName} ${day}, ${year}`;
    if (lang === "id") {
      format = `${day} ${monthName} ${year}`;
    }
  }
  return format;
};

/**
 * Checks if an array of object has certain key
 * @param {Array} arr The array of object
 * @param {String} key The key to check
 * @returns
 */
export const arrayHasKey = (arr, key) => {
  if (!arr || !Array.isArray(arr) || !key) return;

  return arr.some(obj => Object.keys(obj).includes(key));
};

/**
 * Replace spaces with hyphens
 * @param {String} str The string to replace
 * @returns {String}
 */
export const toURLFormat = str => str.replace(/\s+/g, "-").toLowerCase();

/**
 * Checks if an object or array is empty or not
 * @param {Object|Array} data An object or an array
 * @returns {Boolean}
 */
export const isEmpty = data => (data || []).length === 0 || Object.keys(data).length === 0;

/**
 * Adds between two numbers
 * @param {Number} a The first number
 * @param {Number} b The second number
 * @returns {Number}
 */
export const add = (a, b) => a + b;

/**
 * Calculates all the numbers in an array
 * @param {Number[]} arr An array of numbers
 * @returns {Number}
 */
export const sumArr = arr => arr.reduce(add);

/**
 * Removes a value from an array
 * @param {Array} arr The array to be removed from
 * @param {*} valueToRemove index of the value to be removed
 * @returns {Array} The array without the value
 */
export const arrayRemove = (arr, valueToRemove) => {
  return arr.filter(function (ele) {
    return ele !== valueToRemove;
  });
};

/**
 * Scrolls to the top of the page
 * @returns {void}
 */
export const scrollTop = () => window.scrollTo(0, 0);

/**
 * Converts a number to a string by using the current or specified locale.
 * @param {Number} number The number to set the locale on
 * @param {String} locale locale
 * @returns {String}
 */
export const setCurrency = (number, locales = "id-ID") => Number(number).toLocaleString(locales);

/**
 * Checks if a password is strong enough
 * @param {String} password The password
 * @param {Number} [minLength=8] The minimum length of the password
 * @returns {Boolean}
 */
export const isStrongPassword = (password, minLength = 8) => {
  return (
    /[a-z]/.test(password) && // checks for a-z
    /[A-Z]/.test(password) && // checks for a-z
    /[0-9]/.test(password) && // checks for 0-9
    /\W|_/.test(password) && // checks for special char
    password.length >= minLength
  );
};

/**
 * Converts a number to a string with commas
 * @param {Number} number a number
 * @returns {String}
 */
export const numberWithCommas = number => number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

/**
 * Capitalizes the first letter of a string
 * @param {String} str The string to capitalize
 * @param {Boolean} reCheckUppercase if true, it will check if the first letter is uppercase
 * @returns {String} a string with the first letter capitalized
 */
export const capitalize = (str, reCheckUppercase = false) => {
  const splitStr = str?.toLowerCase().split(" ");
  for (let i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);

    if (reCheckUppercase) splitStr[i] = getUppercase(splitStr[i]);
  }

  return splitStr.join(" ");
};

/**
 * Debounces a function
 * @param {Function} callback The function to be ran after the delay
 * @param {Number} wait The delay in milliseconds
 */
export const debounce = (callback, wait) => {
  let timeoutId = null;
  return (...args) => {
    window.clearTimeout(timeoutId);
    timeoutId = window.setTimeout(() => {
      callback.apply(null, args);
    }, wait);
  };
};

/**
 * Clones an object
 * @param {Object} obj The object to be cloned
 * @returns {Object} The new cloned object
 */
export const cloneObj = obj => {
  const toString = Object.prototype.toString;
  let rv;

  switch (typeof obj) {
    case "object":
      if (obj === null) {
        rv = null;
      } else {
        switch (toString.call(obj)) {
          case "[object Array]":
            rv = obj.map(clone);
            break;
          case "[object Date]":
            // Clone the date
            rv = new Date(obj);
            break;
          case "[object RegExp]":
            // Clone the RegExp
            rv = new RegExp(obj);
            break;
          default:
            rv = Object.keys(obj).reduce(function (prev, key) {
              prev[key] = clone(obj[key]);
              return prev;
            }, {});
            break;
        }
      }
      break;
    default:
      rv = obj;
      break;
  }
  return rv;
};

/**
 * Flattens an object to be one level deep
 * @param {Object} obj The object to be flattened
 * @returns {Object} The flattened object
 */
export const flattenObject = obj => {
  const result = {};
  for (const i in obj) {
    if (typeof obj[i] === "object" && !Array.isArray(obj[i])) {
      const temp = flattenObject(obj[i]);
      for (const j in temp) {
        result[i + "." + j] = temp[j];
      }
    } else {
      result[i] = obj[i];
    }
  }
  return result;
};

export const getUppercase = str => {
  const strLow = str.toLowerCase();
  if (strLow.length < 4) return str.toUpperCase();
  return str;
};
