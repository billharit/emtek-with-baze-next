import { add } from "~/utils";

describe("Utils", () => {
  it("should add numbers", () => {
    expect(add(1, 2)).toEqual(3);
  });
});
