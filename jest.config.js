const nextJest = require("next/jest");

const createJestConfig = nextJest({
  // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
  dir: "./"
});

const TEST_REGEX = "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|js?|tsx?|ts?)$";

const customJestConfig = {
  testRegex: TEST_REGEX,
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  transform: {
    // "^.+\\.tsx?$": "ts-jest",
    "^.+\\.js?$": "babel-jest",
    ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "jest-transform-stub"
    // "^.+\\.mjs$": "babel-jest",
  },
  testPathIgnorePatterns: ["<rootDir>/build/", "<rootDir>/node_modules/", "<rootDir>/dist/"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json"],
  moduleNameMapper: {
    "^~(.*)$": "<rootDir>/$1",
    "\\.(css|scss|less)$": "identity-obj-proxy"
    // "^.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "jest-transform-stub"
  },
  testEnvironment: "jest-environment-jsdom"
};
module.exports = createJestConfig(customJestConfig);
