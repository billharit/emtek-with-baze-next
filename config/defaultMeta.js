export const defaultMeta = {
  title: "Suitbaze NEXT",
  description: "suitbaze nextjs",
  url: "https://suitmedia.com",
  image: "https://suitmedia.com/_nuxt/img/logo.6c17d4a.png",
  keywords: "suitbaze suitmedia, suitbaze, suitmedia"
};
