const axios = require("axios");

/** @type {import('next-sitemap').IConfig} */
module.exports = {
  siteUrl: process.env.HOST_URL,
  changefreq: "daily",
  priority: 0.8,
  generateRobotsTxt: true,
  autoLastmod: new Date(),
  exclude: ["/server-sitemap-index.xml"],
  robotsTxtOptions: {
    additionalSitemaps: [
      process.env.HOST_URL + "/server-sitemap-index.xml" // <==== Add here
    ]
  }
};
