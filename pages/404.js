import Image from "next/image";
import { BlankLayout } from "~/layouts/BlankLayout";
import img404 from "~/public/static/img/404.png";

const Custom404 = () => (
  <div
    className="container text-center"
    style={{ position: "relative", minHeight: "513px", padding: "60px 0", maxWidth: "590px" }}
  >
    <Image alt="404 Error" src={img404} layout="responsive" />
  </div>
);

Custom404.props = {
  layout: BlankLayout,
  meta: {
    title: "404 Not Found"
  }
};

export default Custom404;
