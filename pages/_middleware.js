import { NextResponse, NextRequest } from "next/server";
import { redirectRules as rules } from "~/config/redirectRules";

/**
 * https://nextjs.org/docs/advanced-features/middleware
 * @param {NextRequest} request
 */
export function middleware(request) {
  const { pathname } = request.nextUrl;

  const rule = rules.find(({ source }) => source === pathname);
  if (rule) {
    return NextResponse.redirect(new URL(rule.destination, request.url));
  }

  return NextResponse.next();
}
