import { useEffect, useRef } from "react";
import { signIn, signOut, useSession } from "next-auth/react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

import { DefaultLayout } from "~/layouts/DefaultLayout";
import Modal, { ModalFooter } from "~/components/shared/Modal";

import { toggleDarkMode } from "~/redux/features/darkMode/darkModeSlice";
import { setPageTitle } from "~/redux/features/page/pageSlice";

const Home = () => {
  const firstModalRef = useRef();
  const secondModalRef = useRef();

  const dispatch = useDispatch();
  const { darkMode } = useSelector(store => store.darkMode);
  const { pageTitle } = useSelector(store => store.page);

  const { data: session, status } = useSession();

  useEffect(() => {
    dispatch(setPageTitle("HOME"));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="container">
      <h1 className="mt-16">Page: {pageTitle}</h1>
      <button onClick={() => dispatch(toggleDarkMode())}>Dark Mode : {darkMode.toString()}</button>
      <br />
      {session && status === "authenticated" ? <h1>Signed in as {session.user.email}</h1> : <h1>Not signed in.</h1>}

      <br />

      <button className="btn--primary mr-8" onClick={() => firstModalRef.current.open()}>
        Open First Modal
      </button>
      <button className="btn--ghost-primary" onClick={() => secondModalRef.current.open()}>
        Open Second Modal
      </button>

      <Modal ref={firstModalRef} size={"small"}>
        <h1>First Modal</h1>
        <p>Small with footer</p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed dictum diam. Duis feugiat lorem ante, eu
          bibendum neque maximus vitae. Nulla ex eros, euismod varius efficitur at, tempor ut libero. Aenean id magna
          nisl. Fusce vel pharetra leo. Sed ullamcorper libero felis, quis dapibus lectus pharetra fringilla.
          Suspendisse posuere pharetra interdum. Ut lobortis, arcu quis pharetra dignissim, erat est elementum mauris,
          vitae tempus quam magna eu enim. Nulla interdum ullamcorper nisl a mattis. Pellentesque ut orci vitae lacus
          vehicula dictum. Nullam tincidunt lorem non interdum pellentesque. Etiam viverra id lectus non suscipit.
        </p>

        <ModalFooter>
          <button>Action</button>
        </ModalFooter>
      </Modal>

      <Modal ref={secondModalRef} closeAction={false}>
        <h1>Second Modal</h1>
        <p>no close button</p>
        <p>lorem ipsum dolor sit amet</p>
      </Modal>
    </div>
  );
};

Home.props = {
  layout: DefaultLayout
};

export default Home;
