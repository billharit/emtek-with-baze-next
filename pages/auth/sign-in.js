import { getSession, getProviders, getCsrfToken, signIn } from "next-auth/react";
import { useCallback } from "react";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import { useForm } from "react-hook-form";
import { WithContainer } from "~/layouts/WithContainer";

const SignIn = ({ providers, csrfToken }) => {
	const { executeRecaptcha } = useGoogleReCaptcha();

	const {
		register,
		handleSubmit,
		reset,
		formState: { errors }
	} = useForm({
		shouldUnregister: true
	});

	const sendForm = async data => {
		signIn("credentials", data);
		reset({
			email: "",
			password: ""
		});
	};

	const handleSendForm = useCallback(
		e => {
			e.preventDefault();
			if (!executeRecaptcha) {
				console.log("Execute recaptcha not yet available");
				return;
			}
			executeRecaptcha("send_login").then(gReCaptchaToken => {
				console.log(gReCaptchaToken, "response Google reCaptcha server");
				handleSubmit(data => sendForm({ ...data, captcha: gReCaptchaToken }));
			});
		},
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[executeRecaptcha]
	);

	return (
		<>
			<h1>Login Page</h1>
			<form onSubmit={handleSendForm}>
				<input name="csrfToken" type="hidden" defaultValue={csrfToken} />
				<div className="mb-16">
					<input
						className="form-input mr-8"
						{...register("email", {
							required: "Your email please?",
							pattern: {
								value:
									/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
								message: "Please enter a valid email"
							}
						})}
					/>
					<span>{errors.email?.message}</span>
				</div>

				<div className="mb-16">
					<input
						className="form-input mr-8"
						type="password"
						{...register("password", {
							required: "Your password please?",
							minLength: { value: 3, message: "Your password is too short :(" }
						})}
					/>
					<span>{errors.password?.message}</span>
				</div>
				<button type="submit" className="btn--primary">
					Submit
				</button>
			</form>
			<br />
			<button onClick={() => signIn("google")} className="btn--primary">
				Login With Google
			</button>
		</>
	);
};

SignIn.props = {
	layout: WithContainer
};

SignIn.getInitialProps = async context => {
	const { req, res } = context;
	const session = await getSession({ req });

	if (session && res) {
		res.writeHead(302, {
			Location: "/"
		});
		res.end();
		return;
	}

	return {
		session: undefined,
		providers: await getProviders(context),
		csrfToken: await getCsrfToken(context)
	};
};

export default SignIn;
