import axios from "axios";
import BreadCrumbs from "~/components/shared/BreadCrumbs";
import MetaHead from "~/components/shared/HeadMeta";

const NewsDetail = ({ newsData }) => {
  return (
    <>
      <MetaHead meta={{ title: newsData.title, description: newsData.title }} />
      <div className="container" style={{ paddingTop: "20px" }}>
        <BreadCrumbs />
        <h1>{newsData.title}</h1>
        <article dangerouslySetInnerHTML={{ __html: newsData.content }}></article>
      </div>
    </>
  );
};

export async function getServerSideProps(ctx) {
  const response = await axios.get(`/base/ideas/${ctx.params.slug}`, {
    params: { append: "small_image,medium_image" }
  });

  // NewsDetail.props = {
  //   meta: {
  //     title: response.data.data.title
  //   }
  // };

  return {
    props: {
      newsData: response.data.data
    }
  };
}

export default NewsDetail;
