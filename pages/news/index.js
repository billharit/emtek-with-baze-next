import { useState } from "react";
import { Card } from "~/components/news/Card";
import Pagination from "~/components/shared/Pagination";
import useAxios from "~/hooks/useAxios";

const News = () => {
  const { response, isLoading, error } = useAxios({
    method: "GET",
    url: "/base/ideas",
    params: {
      "page[number]": 1,
      "page[size]": 10,
      append: "small_image,medium_image"
    }
  });

  const [page, setPage] = useState({
    currentPage: 1,
    perPage: 4,
    totalPages: 100
  });

  const onPageChange = pageNum => {
    console.log(page);
    console.log(pageNum);
    // setPage(prevState =>( {...prevState, currentPage: pageNum}))
  };

  return (
    <div className="container">
      <h1>News</h1>
      {isLoading ? (
        <span className="spinner"></span>
      ) : (
        <div className="bzg">
          {response &&
            response.data?.map(v => (
              <div key={v.id} className="bzg_c" data-col="m6, l3">
                <Card data={v} path="/news/" />
              </div>
            ))}
        </div>
      )}
      <Pagination
        currentPage={page.currentPage}
        maxVisibleButtons={page.maxVisibleButtons}
        perPage={page.perPage}
        totalPages={page.totalPages}
        onPageChange={onPageChange}
      />
    </div>
  );
};

export default News;
