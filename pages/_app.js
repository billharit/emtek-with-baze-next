import { Router } from "next/router";
import { Provider } from "react-redux";

import { SessionProvider } from "next-auth/react";

import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

import { DefaultLayout } from "~/layouts/DefaultLayout";
import { defaultMeta } from "~/config/defaultMeta";
import { ProgressBar } from "~/utils/progressBar";

import { store, persistor } from "~/redux/store";
import { PersistGate } from "redux-persist/integration/react";

import MetaHead from "~/components/shared/HeadMeta";

// react-datepicker
import "react-datepicker/dist/react-datepicker.css";

// react-slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "~/styles/main.scss";
import "~/config/axiosConfig";

const progress = new ProgressBar({
	size: 4,
	color: "#000",
	className: "progress-bar",
	delay: 100
});

// invokes the progress bar
Router.events.on("routeChangeStart", () => progress.start());
Router.events.on("routeChangeComplete", () => progress.finish());
Router.events.on("routeChangeError", () => progress.finish());

function MyApp({ Component, pageProps, router }) {
	const Layout = Component.props?.layout || DefaultLayout;
	const meta = {
		...defaultMeta,
		...Component.props?.meta
	};

	return (
		<>
			<GoogleReCaptchaProvider
				reCaptchaKey={process.env.RECAPTCHA_SITE_KEY}
				scriptProps={{
					async: false,
					defer: false,
					appendTo: "head",
					nonce: undefined
				}}
			>
				<SessionProvider session={pageProps.session}>
					<Provider store={store}>
						<PersistGate loading={null} persistor={persistor}>
							<MetaHead customMeta={meta} />
							<div id="layout">
								<Layout>
									<Component {...pageProps} />
								</Layout>
							</div>
						</PersistGate>
					</Provider>
				</SessionProvider>
			</GoogleReCaptchaProvider>
		</>
	);
}

export default MyApp;
