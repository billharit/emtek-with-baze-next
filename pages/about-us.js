import Slider from "react-slick";
import { WithContainer } from "~/layouts/WithContainer";

const AboutUs = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  return (
    <>
      <h1>about page</h1>
      <Slider {...settings}>
        {Array(10)
          .fill()
          .map((v, i) => (
            <div key={Math.random() + Date.now()} className="bg-primary pv-48">
              <h3 className="text-center mb-0">{i + 1}</h3>
            </div>
          ))}
      </Slider>
    </>
  );
};

AboutUs.props = {
  layout: WithContainer
};

export default AboutUs;
