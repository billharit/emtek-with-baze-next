import axios from "axios";
import Image from "next/image";
import BreadCrumbs from "~/components/shared/BreadCrumbs";
import MetaHead from "~/components/shared/HeadMeta";

const ArticleDetail = ({ newsData }) => {
  return (
    <>
      <MetaHead meta={{ title: newsData.title, description: newsData.title }} />
      <div className="container" style={{ paddingTop: "20px" }}>
        <BreadCrumbs />
        <h1>{newsData.title}</h1>
        <article dangerouslySetInnerHTML={{ __html: newsData.content }}></article>
        <div style={{ position: "relative", width: "100%", height: "200px" }}>
          <Image src={newsData.small_image} alt="Picture of the author" layout="fill" objectFit="contain" />
        </div>
      </div>
    </>
  );
};

export async function getServerSideProps(ctx) {
  let data = {}
  await axios
    .get(`/articles/${ctx.params.slug}`, {
      params: { append: "small_image" }
    })
    .then((res => {
      data = res.data.data
    }))
    .catch(err => {
      data = err.response;
    });

  if (data.status == 404) {
    return {
      redirect: {
        destination: "/404",
        permanent: false
      }
    };
  }
  if (data.status == 500) {
    return {
      redirect: {
        destination: "/500",
        permanent: false
      }
    };
  }

  return {
    props: {
      newsData: data
    }
  };
}

export default ArticleDetail;
