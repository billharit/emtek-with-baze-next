import { DefaultLayout } from "~/layouts/DefaultLayout";
import { motion } from "framer-motion";
import Slider from "react-slick";
import Image from "next/image";
import gambargedung from "~/public/static/img/gambargedung.jpg";
import gambaralam from "~/public/static/img/nature.jpg";
import { ProfileAnimation } from "~/components/emtekhome/ProfileAnimation";
import { SocialMedia } from "~/components/emtekhome/SocialMedia";

import style from "./emtek.module.scss";

const emtekHome = () => {
  const SliderData = [
    {
      imglink: gambargedung,
      description: (
        <>
          The leading provider and service partner in entertainment, <br /> communication, information, and technology
        </>
      ),
      titleText: (
        <>
          Future Start <br /> Here
        </>
      ),
      button: true
    },
    {
      imglink: gambaralam,
      description: (
        <>
          The leading provider and service partner in entertainment, <br /> communication, information, and technology
        </>
      ),
      titleText: (
        <>
          Future Start <br /> Here
        </>
      ),
      button: true
    },
    {
      imglink: gambargedung,
      description: (
        <>
          The leading provider and service partner in entertainment, <br /> communication, information, and technology
        </>
      ),
      titleText: (
        <>
          Future Start <br /> Here
        </>
      ),
      button: true
    }
  ];

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true
  };
  return (
    <div>
      {/* <section>
        <Slider {...settings}>
          {SliderData.map((slide, index) => {
            return (
              <div className={style["slidercontent"]} key={index}>
                <div className={style["sliderimage"]}>
                  <Image src={slide.imglink} alt="Gedung" layout="fill" objectFit="cover" />
                </div>
                <div className={style["slidertext"] + " " + "text-white"}>
                  <motion.h1
                    initial={{ y: 50, opacity: 0 }}
                    // animate={{ y: 0, opacity: 1 }}
                    whileInView="visible"
                    variants={{
                      visible: {
                        y: 0,
                        opacity: 1,
                        transition: {
                          duration: 1,
                          ease: [0.6, -0.05, 0.01, 0.99]
                        }
                      }
                    }}
                  >
                    {slide.titleText}
                  </motion.h1>
                  <motion.p
                    initial={{ opacity: 0 }}
                    whileInView="visible"
                    variants={{
                      visible: {
                        y: 0,
                        opacity: 1,
                        transition: {
                          delay: 1,
                          duration: 1,
                          ease: [0.6, -0.05, 0.01, 0.99]
                        }
                      }
                    }}
                  >
                    {slide.description}
                  </motion.p>
                  <motion.button
                    className="btn btn--ghost-white"
                    initial={{ y: -25, opacity: 0 }}
                    whileInView="visible"
                    variants={{
                      visible: {
                        y: 0,
                        opacity: 1,
                        transition: {
                          delay: 2,
                          duration: 1,
                          ease: [0.6, -0.05, 0.01, 0.99]
                        }
                      }
                    }}
                  >
                    <i className="">about emtek</i>
                  </motion.button>
                </div>
              </div>
            );
          })}
        </Slider>
      </section> */}
      <section>
        <ProfileAnimation></ProfileAnimation>
      </section>
      <section>
        <SocialMedia></SocialMedia>
      </section>
    </div>
  );
};

emtekHome.props = {
  layout: DefaultLayout
};

export default emtekHome;
