import axios from "axios";
import { getServerSideSitemapIndex } from "next-sitemap";

export const getServerSideProps = async ctx => {
  // get all paths
  const routeSitemap = [];
  const axiosGet = axios.create({
    baseURL: process.env.HOST_URL,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });

  const getData = await Promise.all([axiosGet.get("/base/ideas/")]);

  getData[0].data.data.forEach(i => {
    routeSitemap.push(process.env.HOST_URL + "/news/" + i.slug);
  });

  return getServerSideSitemapIndex(ctx, routeSitemap);
};

// Default export to prevent next.js errors
export default function SitemapIndex() {}
