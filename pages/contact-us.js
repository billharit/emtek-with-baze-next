import ContactForm from "~/components/contact/ContactForm";
import { WithContainer } from "~/layouts/WithContainer";

const ContactUs = () => {
  return <ContactForm />;
};

ContactUs.props = {
  layout: WithContainer,
  meta: {
    title: "Contact Us Page"
  }
};

export default ContactUs;
