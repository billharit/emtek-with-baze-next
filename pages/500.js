import Image from "next/image";
import { BlankLayout } from "~/layouts/BlankLayout";
import img500 from "~/public/static/img/500.png";

const Custom500 = () => (
  <div
    className="container text-center"
    style={{ position: "relative", minHeight: "513px", padding: "60px 0", maxWidth: "590px" }}
  >
    <Image alt="500 Error" src={img500} layout="responsive" />
  </div>
);

Custom500.props = {
  layout: BlankLayout,
  meta: {
    title: "500 Internal Server Error"
  }
};

export default Custom500;
