import { useEffect, useState } from "react";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

/**
 * Simplify axios client-side data fetching
 *
 * @param {AxiosRequestConfig} axiosParams Params for the request
 * @returns {AxiosResponse}  The response from the API
 * @returns {Boolean}  is loading
 * @returns {Error|null}  error
 */
export default function useAxios(axiosParams) {
  const [response, setResponse] = useState(undefined);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = async params => {
    try {
      const result = await axios.request(params);
      setResponse(result.data);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData(axiosParams);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { response, isLoading, error };
}
