import { combineReducers } from "redux";

import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import darkMode from "./features/darkMode/darkModeSlice";
import page from "./features/page/pageSlice";

const darkModePersistConfig = {
	key: "darkMode",
	storage
};

const reducers = combineReducers({
	darkMode: persistReducer(darkModePersistConfig, darkMode),
	page
});

export default reducers;
