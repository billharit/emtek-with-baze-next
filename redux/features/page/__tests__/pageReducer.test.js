import pageReducer, { setPageTitle } from "~/redux/features/page/pageSlice";

describe("Page Reducer", () => {
  it("should return the initial state", () => {
    expect(pageReducer(undefined, { type: undefined })).toEqual({ currentPage: null, pageTitle: "" });
  });

  it("should handle changing page title", () => {
    expect(pageReducer(undefined, setPageTitle("Test"))).toEqual({ currentPage: null, pageTitle: "Test" });
  });
});