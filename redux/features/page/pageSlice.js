import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	currentPage: null,
	pageTitle: ""
};

const pageSlice = createSlice({
	name: "page",
	initialState,
	reducers: {
		setPageTitle(state, action) {
			state.pageTitle = action.payload;
		}
	}
});

export const { setPageTitle } = pageSlice.actions;
export default pageSlice.reducer;
