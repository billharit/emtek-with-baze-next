import darkModeReducer, { toggleDarkMode } from "~/redux/features/darkMode/darkModeSlice";

describe("Dark Mode Reducer", () => {
  it("should return the initial state", () => {
    expect(darkModeReducer(undefined, { type: undefined })).toEqual({ darkMode: false });
  });

  it("should handle being toggled", () => {
    expect(darkModeReducer(undefined, toggleDarkMode())).toEqual({ darkMode: true });
  });
});