import { render } from "@testing-library/react";
import axios from "axios";
import "~/config/axiosConfig";
import { MockReduxProvider } from "~/utils/test-utils";
import ArticleDetail, { getServerSideProps } from "~/pages/[slug]";

const useRouter = jest.spyOn(require("next/router"), "useRouter");
var newsData = {}
async function getData() {
  await axios.get(`/articles/artikel-baru`, {
    params: { append: "small_image" }
  }).then(response => {
    newsData = response.data.data
  });
};

describe("@pages/[slug]", () => {
  useRouter.mockImplementationOnce(() => ({
    asPath: "/artikel-baru"
  }));

  getData();

  it("should use getServerSideProps correctly", async () => {
    const ctx = await getServerSideProps({ params: { slug: "artikel-baru" } });
    expect(ctx.props.newsData).toEqual(newsData);
  });

  it("should render pages", () => {
    const { container } = render(
      <MockReduxProvider>
        <ArticleDetail newsData={newsData} />
      </MockReduxProvider>
    );
    expect(container).toMatchSnapshot();
  });
});
