import { render, screen } from "@testing-library/react";

import { MockReduxProvider } from "~/utils/test-utils";
import Home from "~/pages/index";

// mock next-auth useSession()
jest.mock("next-auth/react", () => {
	const originalModule = jest.requireActual("next-auth/react");
	const mockSession = {
		expires: new Date(Date.now() + 2 * 86400).toISOString(),
		user: { email: "test@admin.com" }
	};

	return {
		__esModule: true,
		...originalModule,
		useSession: jest.fn(() => {
			return { data: mockSession, status: "authenticated" };
		})
	};
});

describe("Home", () => {
	it("renders a heading", () => {
		render(
			<MockReduxProvider>
				<Home />
			</MockReduxProvider>
		);

		const heading = screen.getByRole("heading", {
			name: /Page: Home/i
		});

		expect(heading).toBeInTheDocument();
	});

	it("renders user email", () => {
		render(
			<MockReduxProvider>
				<Home />
			</MockReduxProvider>
		);

		const username = screen.getByText(/test@admin.com/i);
		expect(username).toBeInTheDocument();
	});

	it("renders homepage", () => {
		const { container } = render(
			<MockReduxProvider>
				<Home />
			</MockReduxProvider>
		);
		expect(container).toMatchSnapshot();
	});
});
