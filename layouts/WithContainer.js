import Header from "~/components/shared/Header";
import { Footer } from "~/components/shared/Footer/Footer";

export const WithContainer = ({ children }) => {
  return (
    <>
      <Header />
      <main className="site-main">
        <div className="container">{children}</div>
      </main>
      <Footer />
    </>
  );
};
