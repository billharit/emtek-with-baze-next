import { Footer } from "~/components/shared/Footer/Footer";
import Header from "~/components/shared/Header";

export const DefaultLayout = ({ children }) => {
  return (
    <>
      <Header />
      <main className="site-main">{children}</main>
      <Footer />
    </>
  );
};
