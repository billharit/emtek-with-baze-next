export const BlankLayout = ({ children }) => {
  const layoutStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  };
  return <main style={layoutStyle}>{children}</main>;
};
